import amazoneservices.cognito.CognitoFactory;
import amazoneservices.dynamodb.DynamoFactory;
import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.dynamodbv2.xspec.NULL;
import configuration.CvConfiguration;
import model.Reviews;
import model.Users;
import org.apache.http.util.Asserts;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CvConfiguration.class})
public class CvConnectionTest {

    private Logger log = LoggerFactory.getLogger(CvConnectionTest.class);

    @Autowired
    private CognitoFactory connection;

    @Autowired
    private DynamoFactory dbConnection;

    @Autowired
    private Users utente;

    @Test
    public void getNumListUsersConnection(){
        int numero = connection.listAllUsers(50).size();
        Assert.assertEquals( 1, numero);
    }

    @Test
    public void getNumRecensioniByUser() throws DynamoOperationException {
        int numRecensioniUtente = dbConnection.getListReview("army93").size();
        Assert.assertEquals(6,numRecensioniUtente);
    }

    @Test
    public void getAllReviews() throws DynamoOperationException{
        int numRecensioni = dbConnection.getAllReviews().size();
        Assert.assertEquals(23,numRecensioni);
    }

    public void saveReviews() throws DynamoOperationException {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH)+1;
        String timestamp = calendar.get(Calendar.DAY_OF_MONTH)+"/"+month+"/"+calendar.get(Calendar.YEAR);
        int stelle = new Random().nextInt(5)+1;
        String nomeStruttura="Hotel Esedra";
        String titoloRecensione="Stupendo";
        String dataUltimaVisita="aprile 2018"; //respect format -> maggio 2020, aprile 2012 ...
        String proprietario="AndroidTest";
        String commento="Questo è un test effettuato tramite UnitTest Android per l'inserimento di una recensione per la struttura "+nomeStruttura;

        Reviews reviews = new Reviews(nomeStruttura,
                                        proprietario,
                                        commento,
                                        timestamp,
                                        dataUltimaVisita,
                                        titoloRecensione,
                                        stelle);

        //dbConnection.saveReview(reviews); -- da usare con cautela
    }

    @Test
    public void getAttributeList(){
        List<AttributeType> attributeList = connection.getAttributeList();
        //get attribute nickname from list
        Assert.assertEquals("nickname",attributeList.get(3).getName());
    }

    @Test
    public void checkUserExists(){
        Users userToFindByUsername = new Users("Army93","Armando","Gioielli","armando@mail.com");

        List<Users> listUsers = connection.listAllUsers(60);
        listUsers.removeIf(utente -> !utente.getUsername().equalsIgnoreCase(userToFindByUsername.getUsername()));

        Assert.assertEquals(1,listUsers.size());
    }

    @Test
    public void getReviewByStructureAndUser() throws DynamoOperationException {
        Reviews reviews = dbConnection.getReviewsFromDB("Hotel Esedra","army93");
        Asserts.notNull(reviews,reviews.getNomeUtente());
    }

    @Test
    public void deleteReview() throws DynamoOperationException {
        String nomeStruttura="Hotel Esedra";
        String nomeUtente="army93";
        String titolo = "uahag";
        String ultimaVisita="aprile 2020";
        int valutazione=4;

        Reviews reviews = new Reviews(nomeStruttura,titolo,valutazione,ultimaVisita);
        //dbConnection.deleteReview(reviews); -- da usare con cautela

        Reviews confirm = dbConnection.getReviewsFromDB(nomeStruttura,nomeUtente);
        if(confirm == null){
            log.error("Recensione non trovata");
        }
        else
            Asserts.notNull(confirm, confirm.getNomeUtente());
    }

    @Test
    public void getNomeUtente(){
        List<Users> utenti = connection.listAllUsers(60);
        for(Users u : utenti){
            if(u.getUsername().equalsIgnoreCase("army93"));
            utente = u;
        }
        Assert.assertEquals("army93",utente.getUsername());
    }

    @Test
    public void getCognomeUtente(){
        List<Users> utenti = connection.listAllUsers(60);
        for(Users u : utenti){
            if(u.getUsername().equalsIgnoreCase("army93"));
            utente = u;
        }
        Assert.assertEquals("army93",utente.getCognome());
    }

    @Test
    public void getEmailUtente(){
        List<Users> utenti = connection.listAllUsers(60);
        for(Users u : utenti){
            if(u.getUsername().equalsIgnoreCase("army93"));
            utente = u;
        }
        Assert.assertEquals("army93@mail.com",utente.getEmail());
    }


}
