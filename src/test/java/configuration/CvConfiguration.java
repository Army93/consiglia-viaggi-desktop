package configuration;

import amazoneservices.cognito.CognitoFactory;
import amazoneservices.dynamodb.DynamoFactory;
import model.Users;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CvConfiguration {

    @Bean
    CognitoFactory connection (){return new CognitoFactory();}

    @Bean
    DynamoFactory dbConnect(){return new DynamoFactory();}

    @Bean
    Users utente(){return new Users();}


}
