package amazoneservices.dynamodb.interfaces;

import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import model.Reviews;

import java.util.List;

public interface IDynamoConnection {

    /**
     * Ottiene una recensione da DynamoDB in base ai parametri key e sort key
     * @param nomeStruttura Nome della struttura recensita
     * @param nomeUtente Nome dell'utente proprietario della recensione
     * @return Oggetto Recensione
     * @throws DynamoOperationException
     */
    Reviews getReviewsFromDB(String nomeStruttura, String nomeUtente) throws DynamoOperationException;

    /**
     * Salva una recensione sul DB (In caso di implementazioni future)
     * @param review
     * @throws DynamoOperationException
     */
    void saveReview(Reviews review) throws DynamoOperationException;

    /**
     * Elimina una recensione dal DB
     * @param recensione Oggetto Recensione da eliminare
     * @throws DynamoOperationException
     */
    void deleteReview(Reviews recensione) throws DynamoOperationException;

    /**
     * Ottiene la lista di tutte le recensioni presenti sul Database filtrate per username
     * @param nomeUtente Username dell'utente
     * @return Lista di recensioni dell'utente
     * @throws DynamoOperationException
     */
    List<Reviews> getListReview(String nomeUtente) throws DynamoOperationException;

    /**
     * Ottiene la lista di tutte le recensioni presenti sul DB non filtrate
     * @return Lista di tutte le recensioni presenti
     * @throws DynamoOperationException
     */
    List<Reviews> getAllReviews() throws DynamoOperationException;

}
