package amazoneservices.dynamodb;

import amazoneservices.dynamodb.exceptions.DynamoDbException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.amplify.model.App;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class DynamoConnection {

    private static Logger log = LoggerFactory.getLogger(DynamoConnection.class);

    private static final String TABLE_NAME="Recensioni";
    private static DynamoConnection dynamoConnection = null;
    private AmazonDynamoDB dbClient;

    //Costruttore invocabile solo tramite il metodo getInstance
    private DynamoConnection(String AwsAccessKey, String AwsSecretKey, String regionName){

        BasicAWSCredentials credentials = new BasicAWSCredentials(AwsAccessKey,AwsSecretKey);
        Regions region = Regions.fromName(regionName);

        try {
            //Avvio la procedura di connessione

            dbClient = AmazonDynamoDBClientBuilder
                    .standard()
                    .withRegion(region)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();

            log.trace("Connessione al DB stabilita con successo");

        }catch (AmazonServiceException a){
            throw new DynamoDbException("Errore durante la connessione con DynamoDB");
        }

    }

    /**
     * Ottieni un istanza di connessione a Dynamo
     * @return DynamoDB Connection
     */
    public static DynamoConnection getInstance(){

        if(dynamoConnection == null){
            Properties properties = new Properties();
            try {
                properties.load(App.class.getClassLoader().getResourceAsStream("Properties/AwsCredentials.properties"));

                return new DynamoConnection(properties.getProperty("AWS_ACCESS_KEY"),
                        properties.getProperty("AWS_SECRET_KEY"),
                        properties.getProperty("AWS_REGION"));
            } catch (IOException e) {
                log.error("Errore nel recuperare il file di configurazione aws");
                throw new DynamoDbException("Errore durante il recupero delle credenziali");
            }
        }
        return dynamoConnection;
    }

    /**
     * Ottieni il nome della tabella di DynamoDB
     * @return
     */
    public String getTableName() {
        return TABLE_NAME;
    }

    /**
     * Client da utilizzare per le operazioni sulle tabelle del db
     * @return dbClient
     */
    public AmazonDynamoDB getDbClient() {
        return dbClient;
    }
}
