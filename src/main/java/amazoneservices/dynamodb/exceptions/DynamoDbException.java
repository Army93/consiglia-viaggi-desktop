package amazoneservices.dynamodb.exceptions;

import com.amazonaws.AmazonServiceException;

public class DynamoDbException extends AmazonServiceException {
    /**
     * Eccezione generata durante le chiamate al DB Dynamo
     * @param message Messaggio contenente l'errore scaturito
     */
    public DynamoDbException (String message){
        super(message);
    }
}
