package amazoneservices.dynamodb.exceptions;

public class DynamoOperationException extends Exception {
    /**
     * Eccezione generata durante le operazioni dei dati ottenuti da DynamoDB
     * @param message Messaggio contenente l'errore scaturito
     */
    public DynamoOperationException (String message){
        super(message);
    }
}
