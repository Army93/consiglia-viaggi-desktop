package amazoneservices.dynamodb.operations;

import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import model.Reviews;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.List;

public class UtilsOperations {

    private static Logger log = LoggerFactory.getLogger(UtilsOperations.class);

    /**
     * Calcola il voto medio delle recensioni
     * @param listaUtente Lista delle recensioni dell'utente specifico
     * @return Media voto recensioni
     */
    public static float getMediaRecensioni(List<Reviews> listaUtente){
        float media=0;

        //Utente senza recensioni
        if(listaUtente.size() == 0){
            log.warn("Media non calcolata o nessuna recensione presente");
            return media;
        }

        for(Reviews reviews: listaUtente){
            media += reviews.getValutazioneRecensione();
        }

        //Calcolo la media
        media=media/listaUtente.size();

        log.info("Media recensioni di "+listaUtente.get(0).getNomeUtente()+": "+media);

        return media;

    }

    /**
     * Crea la lista delle recensioni del singolo utente
     * @param listaRecensioni Lista di tutte le recensioni ottenute da DynamoDB
     * @param nomeUtente Nome dell'utente proprietario della recensione
     * @return Lista filtrata in base all'utente
     */
    public static List<Reviews> getReviewsUser(List<Reviews> listaRecensioni, String nomeUtente){
    List<Reviews> listaRecensioniUtente= new ArrayList<>();

    for (Reviews reviews : listaRecensioni){
        if(reviews.getNomeUtente().equalsIgnoreCase(nomeUtente)){
            listaRecensioniUtente.add(reviews);
        }
    }

    if(listaRecensioniUtente.size()==0){
        log.warn("Nessuna recensione per l'utente "+nomeUtente);
    }

    return listaRecensioniUtente;
    }

    /**
     * Ottiene il numero di recensioni filtrate per utente
     * @param listaRecensioni Lista di tutte le recensioni ottenute da DynamoDB
     * @return Numero di recensioni
     * @throws DynamoOperationException Se la lista passata come parametro è vuota solleva l'eccezione
     */
    public static int getNumeroRecensioniTotali(List<Reviews> listaRecensioni) throws DynamoOperationException {

        if(listaRecensioni.size() == 0){
            log.warn("Nessuna recensione per l'utente ");
            return 0;
        }

        return listaRecensioni.size();
    }

}
