package amazoneservices.dynamodb;

import amazoneservices.dynamodb.exceptions.DynamoDbException;
import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import amazoneservices.dynamodb.interfaces.IDynamoConnection;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.*;
import javafx.beans.binding.IntegerBinding;
import model.Reviews;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DynamoFactory implements IDynamoConnection {

    private Logger log = LoggerFactory.getLogger(DynamoFactory.class);

    private DynamoConnection dynamoConnection;
    private DynamoDBMapper mapper;

    /**
     * Ottengo la connessione per poter eseguire le operazioni
     */
    public DynamoFactory(){
        dynamoConnection = DynamoConnection.getInstance();
        mapper = new DynamoDBMapper(dynamoConnection.getDbClient());
    }


    /**
     * Ottiene una recensione da DynamoDB in base ai parametri key e sort key
     * @param nomeStruttura Nome della struttura recensita
     * @param nomeUtente Nome dell'utente proprietario della recensione
     * @return Oggetto Recensione
     * @throws DynamoOperationException
     */
    public Reviews getReviewsFromDB(String nomeStruttura, String nomeUtente) throws DynamoOperationException {
        try {
            Reviews recensione = mapper.load(Reviews.class, nomeStruttura, nomeUtente);

            if(recensione == null){
            log.error("Recensione inesistente");
            throw new DynamoOperationException("Errore Recensione non trovata o inesistente");
            }
            log.info("Recensione ottenuta con successo..");

            return recensione;

        }catch (AmazonDynamoDBException e){
            log.error("Errore");
            throw new DynamoOperationException("Errore durante la chiamata al DB per ottenere la recensione");
        }
    }

    /**
     * Salva una recensione sul DB (In caso di implementazioni future)
     * @param review
     * @throws DynamoOperationException
     */
    public void saveReview(Reviews review) throws DynamoOperationException {
        try {
            mapper.save(review);
            log.info("Recensione salvata con successo");
        }catch (AmazonServiceException a){
            throw new DynamoOperationException("Errore durante il salvataggio della recensione sul DB");
        }


    }

    /**
     * Elimina una recensione dal DB
     * @param recensione Oggetto Recensione da eliminare
     * @throws DynamoOperationException
     */
    public void deleteReview(Reviews recensione) throws DynamoOperationException {

        if(recensione == null){
            log.error("Recensione non valida");
            throw new DynamoOperationException("Errore Recensione non trovata o inesistente");
        }

        try{

        mapper.delete(recensione);


        }catch (AmazonDynamoDBException e){
            log.error("Errore eliminazione");
            throw new DynamoOperationException("Errore durante la delete della recensione dal DB");
        }
    }


    /**
     * Ottengo la lista di tutte le recensioni presenti sul Database filtrate per username
     * @param nomeUtente Username dell'utente
     * @return Lista di recensioni dell'utente
     * @throws DynamoOperationException
     */
    public List<Reviews> getListReview(String nomeUtente) throws DynamoOperationException {

        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();

        expressionAttributeValues.put(":val", new AttributeValue().withS(nomeUtente));

        try {

            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(dynamoConnection.getTableName())
                    .withFilterExpression("NomeUtente = :val")
                    .withExpressionAttributeValues(expressionAttributeValues);

            ScanResult result = dynamoConnection.getDbClient().scan(scanRequest);

            List<Map<String, AttributeValue>> items = result.getItems();

            List<Reviews> listReviews = new ArrayList<>();

            for(Map<String, AttributeValue> map : items){
                listReviews.add(new Reviews(
                        map.get("NomeStruttura").getS(),
                        map.get("NomeUtente").getS(),
                        map.get("Commento").getS(),
                        map.get("DataRecensione").getS(),
                        map.get("DataUltimaVisita").getS(),
                        map.get("Titolo").getS(),
                        Integer.parseInt(map.get("Valutazione").getN())));
            }

            return listReviews;

        }catch (AmazonServiceException e){
            throw new DynamoOperationException("Errore durante la get della lista di Recensioni da DynamoDB");
        }

    }

    /**
     * Ottiene la lista di tutte le recensioni presenti sul DB non filtrate
     * @return Lista di tutte le recensioni presenti
     * @throws DynamoOperationException
     */
    public List<Reviews> getAllReviews() throws DynamoOperationException{
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();

        try {

            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(dynamoConnection.getTableName());

            ScanResult result = dynamoConnection.getDbClient().scan(scanRequest);

            List<Map<String, AttributeValue>> items = result.getItems();

            List<Reviews> listReviews = new ArrayList<>();

            for(Map<String, AttributeValue> map : items){
                listReviews.add(new Reviews(
                        map.get("NomeStruttura").getS(),
                        map.get("NomeUtente").getS(),
                        map.get("Commento").getS(),
                        map.get("DataRecensione").getS(),
                        map.get("DataUltimaVisita").getS(),
                        map.get("Titolo").getS(),
                        Integer.parseInt(map.get("Valutazione").getN())));
            }

            return listReviews;

        }catch (AmazonServiceException e){
            throw new DynamoOperationException("Errore durante la get della lista di Recensioni da DynamoDB");
        }
    }
}
