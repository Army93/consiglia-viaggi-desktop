package amazoneservices.cognito.interfaces;

import amazoneservices.cognito.exceptions.ConnectionActionsException;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.UserType;
import model.Users;

import java.util.List;

public interface CognitoInterface {

    /**
     * Lista di utenti iscritti sull'app mobile ConsigliaViaggi Mobile
     * @param limit Numero massimo di iscritti ottenibili dalla lista
     * @return Lista di utenti
     */
    List<Users> listAllUsers(int limit);

    /**
     * Converte la lista di ListUserRequest in Lista di Users
     * @param awsCognitoUser Lista di UserRequest
     * @return Lista di utenti
     */
    Users convertCognitoUser(UserType awsCognitoUser);

    /**
     * Modifica l'utente in base ai nuovi attributi passati nella Lista
     * @param attributi Lista di attributi da aggiornare
     * @param user Utente da aggiornare
     */
    void updateUser(List<AttributeType> attributi, Users user) throws ConnectionActionsException;

    /**
     * Ottieni la lista degli attributi associati agli utenti dell'userpool di Cognito
     * @return Lista di AttributeType
     */
    List<AttributeType> getAttributeList();

    /**
     * Elima utente dallo userpool di Cognito
     * @param user utente da eliminare
     */
    void deleteUser(Users user) throws ConnectionActionsException;

    /**
     * Reset password user
     * @param user Username per il reset password
     */
    void resetPassword(Users user) throws ConnectionActionsException;

    }
