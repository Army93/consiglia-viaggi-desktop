package amazoneservices.cognito;

import amazoneservices.cognito.connection.CognitoAuthentication;
import amazoneservices.cognito.exceptions.ConnectionActionsException;
import amazoneservices.cognito.interfaces.CognitoInterface;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.cognitoidp.model.*;
import model.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class CognitoFactory implements CognitoInterface {

    private Logger log = LoggerFactory.getLogger(CognitoFactory.class);
    private CognitoAuthentication connection;
    private Properties properties;

    public CognitoFactory(){
        try {
            //Connessione
            this.connection = CognitoAuthentication.getConnectionInstance();
            //Dati per le chiamate a Cognito
            properties = connection.getProperties();

        } catch (ConnectionActionsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ottieni la lista di tutti gli utenti registrati su Amazon Cognito
     * @param limit
     * @return
     */
    public List<Users> listAllUsers(int limit) {
        /** check limit 0<limit<=60 "Maximum number of users to be returned" */
        if (limit <= 0 || limit > 60) {
            throw new IllegalArgumentException("limit must have a value less than or equal to 60");
        }

        List<Users> users = new ArrayList<>();

        /** prepare Cognito list users request */
        ListUsersRequest listUsersRequest = new ListUsersRequest();
        listUsersRequest.withUserPoolId(properties.getProperty("AWS_USER_POOL_ID"));
        listUsersRequest.setLimit(limit);

        /** send list users request */
        ListUsersResult result = connection.getClient().listUsers(listUsersRequest);

        List<UserType> userTypeList = result.getUsers();

        for(UserType typelist : userTypeList){
            users.add(convertCognitoUser(typelist));
        }

        return users;
    }

    /**
     * Estrai utente dalla lista di usertypes
     * @param awsCognitoUser
     * @return
     */
    public Users convertCognitoUser(UserType awsCognitoUser) {

        List<AttributeType> type = awsCognitoUser.getAttributes();
        Users user = new Users();
        user.setNome(type.get(2).withName("name").getValue());
        user.setCognome(type.get(4).withName("family_name").getValue());
        user.setUsername(type.get(3).withName("nickname").getValue());
        user.setEmail(type.get(5).withName("email").getValue());

        return user;
    }

    /**
     * Modifica l'utente in base ai nuovi attributi passati nella Lista
     * @param attributi Lista di attributi da aggiornare
     * @param user Utente da aggiornare
     */
    public void updateUser(List<AttributeType> attributi, Users user) throws ConnectionActionsException {
        try {

            log.trace("Avvio update utente");
            AdminUpdateUserAttributesRequest request = new AdminUpdateUserAttributesRequest();
            request.setUsername(user.getUsername());
            request.setUserAttributes(attributi);
            request.setUserPoolId(properties.getProperty("AWS_USER_POOL_ID"));

            connection.getClient().adminUpdateUserAttributes(request);

            log.info("Utente aggiornato con successo..");

        }catch (AmazonServiceException e){
            throw new ConnectionActionsException("Errore durante l'update dell'utente");
        }
    }

    /**
     * Ottieni la lista degli attributi associati agli utenti dell'userpool di Cognito
     * @return Lista di AttributeType
     */
    public List<AttributeType> getAttributeList(){
        /** prepare Cognito list users request */
        ListUsersRequest listUsersRequest = new ListUsersRequest();
        listUsersRequest.withUserPoolId(properties.getProperty("AWS_USER_POOL_ID"));
        listUsersRequest.setLimit(1);

        /** send list users request */
        ListUsersResult result = connection.getClient().listUsers(listUsersRequest);

        List<UserType> userTypeList = result.getUsers();

        return userTypeList.get(0).getAttributes();
    }

    /**
     * Elima utente dallo userpool di Cognito
     * @param user utente da eliminare
     */
    public void deleteUser(Users user) throws ConnectionActionsException {
        try {

            log.trace("Elimino l'utente "+user.getUsername());
            AdminDeleteUserRequest request = new AdminDeleteUserRequest();
            request.setUsername(user.getUsername());
            request.setUserPoolId(properties.getProperty("AWS_USER_POOL_ID"));
            connection.getClient().adminDeleteUser(request);

            log.info("Utente eliminato con successo...");

        }catch (AmazonServiceException s){
            log.error("Errore: utente non eliminato");
            throw new ConnectionActionsException("Errore durante l'eliminazione dell'utente'");
        }
        }

    /**
     * Reset password user
     * @param user Username per il reset password
     */
    public void resetPassword(Users user) throws ConnectionActionsException {
        try {
        AdminResetUserPasswordRequest request = new AdminResetUserPasswordRequest();
        request.setUsername(user.getUsername());
        request.setUserPoolId(properties.getProperty("AWS_USER_POOL_ID"));
        connection.getClient().adminResetUserPassword(request);
        }catch (AmazonServiceException s){
            log.error("Errore: Password utente non cambiata");
            throw new ConnectionActionsException("Errore durante il reset password dell' utente");
        }
    }

}
