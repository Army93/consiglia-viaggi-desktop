package amazoneservices.cognito.connection;

import amazoneservices.cognito.exceptions.ConnectionActionsException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.amplify.model.App;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class CognitoAuthentication {

    private Logger log = LoggerFactory.getLogger(CognitoAuthentication.class);

    private static CognitoAuthentication connection= null;
    private Properties properties;
    private AWSCognitoIdentityProvider identityProvider;

    private CognitoAuthentication() throws ConnectionActionsException {
        //read from properties file
        properties = new Properties();

        try{
            properties.load(App.class.getClassLoader().getResourceAsStream("Properties/AwsCredentials.properties"));
        } catch (IOException e) {
            log.error("Errore durante la lettura delle credenziali nel file properties");
           throw new ConnectionActionsException("Errore durante la procedura di connessione con Cognito");
        }

        BasicAWSCredentials creds = new BasicAWSCredentials(
                properties.getProperty("AWS_ACCESS_KEY"),
                properties.getProperty("AWS_SECRET_KEY"));

        AWSCognitoIdentityProviderClientBuilder builder = AWSCognitoIdentityProviderClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(creds));
        builder.setRegion(properties.getProperty("AWS_REGION"));

        this.identityProvider = builder.build();
    }

    /**
     * Crea la connessione con Cognito
     * @return istanza di connessione
     */
    public static CognitoAuthentication getConnectionInstance() throws ConnectionActionsException {
        if(connection == null){
            return new CognitoAuthentication();
        }
        return connection;
    }

    /**
     * Istanza di connessione per le operations solo dopo aver stabilito la connessione
     * @return AWSCognitoProvider client
     */
    public AWSCognitoIdentityProvider getClient(){
            return this.identityProvider;
    }

    /**
     * Ritorna le properties per ottenere i dati di accesso per le operazioni di estrazione
     * su Cognito
     * @return Properties contenente i dati di accesso
     */
    public Properties getProperties(){
        if (properties != null) {
            return this.properties;
        }
        log.error("Properties null");

        return null;
    }

}