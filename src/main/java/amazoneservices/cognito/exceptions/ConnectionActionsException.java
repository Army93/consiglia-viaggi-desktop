package amazoneservices.cognito.exceptions;

public class ConnectionActionsException extends Exception {

    /**
     * Eccezione generata durante le azioni in uscita verso amazon Service
     * @param message Messaggio contenente l'errore scaturito
     */
    public ConnectionActionsException (String message){
        super(message);
    }
}
