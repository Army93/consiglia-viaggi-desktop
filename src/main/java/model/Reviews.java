package model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Recensioni")
public class Reviews {
    private String nomeStruttura;
    private String nomeUtente;
    private String commento;
    private String dataRecensione;
    private String dataUltimaVisita;
    private String titoloRecensione;
    private int valutazioneRecensione;

    //Costruttore senza argomenti
    public Reviews(){}

    public Reviews(String nomeStruttura, String titoloRecensione, int valutazioneRecensione, String dataUltimaVisita) {
        this.nomeStruttura = nomeStruttura;
        this.dataUltimaVisita = dataUltimaVisita;
        this.titoloRecensione = titoloRecensione;
        this.valutazioneRecensione = valutazioneRecensione;
    }

    //Costruttore con tutti gli argomenti
    public Reviews(String nomeStruttura, String nomeUtente, String commento, String dataRecensione, String dataUltimaVisita, String titoloRecensione, int valutazioneRecensione) {
        this.nomeStruttura = nomeStruttura;
        this.nomeUtente = nomeUtente;
        this.commento = commento;
        this.dataRecensione = dataRecensione;
        this.dataUltimaVisita = dataUltimaVisita;
        this.titoloRecensione = titoloRecensione;
        this.valutazioneRecensione = valutazioneRecensione;
    }

    @DynamoDBHashKey(attributeName="NomeStruttura")
    public String getNomeStruttura() {
        return nomeStruttura;
    }

    public void setNomeStruttura(String nomeStruttura) {
        this.nomeStruttura = nomeStruttura;
    }

    @DynamoDBRangeKey(attributeName = "NomeUtente")
    public String getNomeUtente() {
        return nomeUtente;
    }

    public void setNomeUtente(String nomeUtente) {
        this.nomeUtente = nomeUtente;
    }

    @DynamoDBAttribute(attributeName="Commento")
    public String getCommento() {
        return commento;
    }

    public void setCommento(String commento) {
        this.commento = commento;
    }

    @DynamoDBAttribute(attributeName="DataRecensione")
    public String getDataRecensione() {
        return dataRecensione;
    }

    public void setDataRecensione(String dataRecensione) {
        this.dataRecensione = dataRecensione;
    }

    @DynamoDBAttribute(attributeName="DataUltimaVisita")
    public String getDataUltimaVisita() {
        return dataUltimaVisita;
    }

    public void setDataUltimaVisita(String dataUltimaVisita) {
        this.dataUltimaVisita = dataUltimaVisita;
    }

    @DynamoDBAttribute(attributeName="Titolo")
    public String getTitoloRecensione() {
        return titoloRecensione;
    }

    public void setTitoloRecensione(String titoloRecensione) {
        this.titoloRecensione = titoloRecensione;
    }

    @DynamoDBAttribute(attributeName="Valutazione")
    public int getValutazioneRecensione() {
        return valutazioneRecensione;
    }

    public void setValutazioneRecensione(int valutazioneRecensione) {
        this.valutazioneRecensione = valutazioneRecensione;
    }
}
