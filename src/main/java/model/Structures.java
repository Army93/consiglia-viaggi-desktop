package model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * In caso di implementazione dell'app di amministrazione,
 * oggetto struttura già presente (per future implementazioni)
 */

@DynamoDBTable(tableName = "Strutture")
public class Structures {
    private double latitudine;
    private double longitudine;
    private String nomeStruttura;
    private String indirizzoStruttura;
    private String descrizione;
    private float prezzo;
    private int stelle;

    public Structures(){}

    @DynamoDBHashKey(attributeName="Latitudine")
    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    @DynamoDBRangeKey(attributeName = "Longitudine")
    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    @DynamoDBAttribute(attributeName="Nome")
    public String getNomeStruttura() {
        return nomeStruttura;
    }

    public void setNomeStruttura(String nomeStruttura) {
        this.nomeStruttura = nomeStruttura;
    }

    @DynamoDBAttribute(attributeName="Indirizzo")
    public String getIndirizzoStruttura() {
        return indirizzoStruttura;
    }

    public void setIndirizzoStruttura(String indirizzoStruttura) {
        this.indirizzoStruttura = indirizzoStruttura;
    }

    @DynamoDBAttribute(attributeName="Descrizione")
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @DynamoDBAttribute(attributeName="Prezzo")
    public float getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(float prezzo) {
        this.prezzo = prezzo;
    }

    @DynamoDBAttribute(attributeName="Stelle")
    public int getStelle() {
        return stelle;
    }

    public void setStelle(int stelle) {
        this.stelle = stelle;
    }
}
