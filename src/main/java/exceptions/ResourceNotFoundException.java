package exceptions;

public class ResourceNotFoundException extends Exception {
    /**
     * Solleva un'eccezione se gli oggetti non vengono passati da una View all'altra
     * @param message Descrizione del problema
     */
    public ResourceNotFoundException (String message){
        super(message);
    }
}
