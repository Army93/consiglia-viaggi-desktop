package exceptions;

import java.io.IOException;

public class ViewNotFoundException extends IOException {

    /**
     * Solleva un'eccezione se non trovi i file delle views in /res
     * @param message Descrizione del problema
     */
    public ViewNotFoundException (String message){
        super(message);
    }
}
