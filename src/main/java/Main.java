import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main extends Application{

    private Logger log = LoggerFactory.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) throws IOException {

        log.trace("Entro in Login Scene");

        Parent root = FXMLLoader.load(getClass().getResource("Layout/Login.fxml"));
        Scene scene = new Scene(root,600,350);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Consiglia Viaggi Administration");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Icon/ic_launcher.png")));
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
