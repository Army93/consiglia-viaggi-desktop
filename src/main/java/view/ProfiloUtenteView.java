package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ProfiloUtenteView extends Application {

    private Logger log = LoggerFactory.getLogger(ProfiloUtenteView.class);

    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent userView =  FXMLLoader.load(getClass().getResource("/Layout/ProfiloUtente.fxml"));
        Scene scene = new Scene(userView, 600,400);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Profilo Utente");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Icon/ic_launcher.png")));

        log.trace("Mostro il profilo utente");

        primaryStage.show();
    }

}
