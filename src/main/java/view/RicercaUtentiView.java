package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RicercaUtentiView extends Application {

    private Logger log = LoggerFactory.getLogger(RicercaUtentiView.class);

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent userView =  FXMLLoader.load(getClass().getResource("/Layout/RicercaUtenti.fxml"));
        Scene scene = new Scene(userView, 600,400);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Lista Utenti");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Icon/ic_launcher.png")));


        primaryStage.show();
    }
}
