package controllers;

import com.amazonaws.services.amplify.model.App;
import exceptions.ViewNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.RicercaUtentiView;
import view.components.Dialogs;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    private static Logger log = LoggerFactory.getLogger(LoginController.class);

    private Properties properties = new Properties();

    private RicercaUtentiView view;

    @FXML private TextField usernameField;

    @FXML private PasswordField passwordField;

    @FXML private Button loginButton;

    @FXML private Button exitLogin;

    @FXML
    private void handleButtonLoginAction(ActionEvent event) throws Exception {
        log.info("Login button clicccato");

        if(usernameField.getText().isEmpty() || passwordField.getText().isEmpty()){
            log.trace("Errore: compilare tutti i campi");
        }

        //Leggi i dati da /res/
        try {

            properties.load(App.class.getClassLoader().getResourceAsStream("Properties/adminCredentials.properties"));

            String password = properties.getProperty("psw");

            //Decode Password String
            byte[] byteArray = Base64.decodeBase64(password.getBytes());
            String decodedString = new String(byteArray);

            //Login Successfull
            if(usernameField.getText().equals(properties.getProperty("user")) && passwordField.getText().equals(decodedString)){
                log.info("Accesso effettuato");

                Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                view.start(stage);

                if(view == null){
                    log.error("Errore: non è stata istanziata la prossima scena");
                    throw new ViewNotFoundException("Errore scena successiva non istanziata");
                }
            }
            else {
                log.error("Credenziali non valide");
                Dialogs.showErrorDialog("Credenziali errate","Attenzione, inserite credenziali non valide");
            }

        }catch (IOException e){
            throw new ViewNotFoundException("Errore risorsa non trovata in resources");
        }

    }

    @FXML
    public void handleButtonExitAction(ActionEvent actionEvent) {
        Stage stage = (Stage) exitLogin.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @FXML
    public void onEnter(ActionEvent enterEvent){
        try {
            handleButtonLoginAction(enterEvent);
        } catch (Exception e) {
            log.error("Errore durante la chiamata al metodo per il login");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        view = new RicercaUtentiView();
    }


}
