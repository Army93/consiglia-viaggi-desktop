package controllers;

import amazoneservices.cognito.CognitoFactory;
import amazoneservices.cognito.exceptions.ConnectionActionsException;
import amazoneservices.dynamodb.DynamoFactory;
import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import amazoneservices.dynamodb.operations.UtilsOperations;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import exceptions.ResourceNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Reviews;
import model.Users;
import org.controlsfx.control.Rating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.components.Dialogs;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class ProfiloUtenteController {

    private Logger log = LoggerFactory.getLogger(ProfiloUtenteController.class);
    private DynamoFactory factory = new DynamoFactory();;
    private  List<Reviews> listaRecensioni;
    private Users utenteCorrente;

    @FXML private TextField nomeUtente;
    @FXML private TextField cognomeUtente;
    @FXML private TextField usernameUtente;
    @FXML private TextField emailUtente;

    @FXML private Button resetPasswordButton;
    @FXML private Button indietroButton;
    @FXML private Button salvaButton;
    @FXML private Button annullaButton;
    @FXML private Button modificaUtenteButton;
    @FXML private Button eliminaUtenteButton;
    @FXML private Button visualizzaRecensioniButton;

    @FXML public Rating ratingAvgUser;

    @FXML private Label numRecensioni;

    /**
     * Metodo per inizializzare il controller
     * @param utente
     */
    public void initialize(Users utente){

        utenteCorrente = utente;

        //Enrich TextFields and set disable
        nomeUtente.setText(utenteCorrente.getNome());
        cognomeUtente.setText(utenteCorrente.getCognome());
        usernameUtente.setText(utenteCorrente.getUsername());
        emailUtente.setText(utenteCorrente.getEmail());

        nomeUtente.setEditable(false);
        cognomeUtente.setEditable(false);
        usernameUtente.setEditable(false);
        emailUtente.setEditable(false);

        execute();
    }

    /**
     * Metodo Execute per il processo principale
     */
    public void execute() {
        log.trace("Entro nel profilo utente di "+utenteCorrente.getUsername());

        try {

            listaRecensioni = factory.getListReview(utenteCorrente.getUsername());

            log.info("Recuperate "+listaRecensioni.size()+" recensioni per l'utente selezionato");

            //Calcola e aggiungi media recensioni e numero recensioni dell'utente
            numRecensioni.setText(String.valueOf(UtilsOperations.getNumeroRecensioniTotali(listaRecensioni)));
            ratingAvgUser.setRating(UtilsOperations.getMediaRecensioni(listaRecensioni));

        } catch (DynamoOperationException e) {
            log.error("Errore durante la chiamata per la lista delle recensioni");
        }
    }

    /**
     * Torna alla scena precedente
     * @param actionEvent
     * @throws IOException
     */
    public void onBackButton(ActionEvent actionEvent) throws IOException {
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/Layout/RicercaUtenti.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        log.trace("Torno alla scena precedente");
        Stage current = (Stage) indietroButton.getScene().getWindow();
        current.close();
        stage.show();
    }

    /**
     * Quando si clicca sul tasto visualizza recensioni, si passa la lista delle recensioni alla prossima action
     * @param actionEvent
     */
    public void onViewReviews (ActionEvent actionEvent) throws IOException {
        //Passa allo scenario successivo passando la lista delle recensioni

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Layout/ListaRecensioni.fxml"));

        Parent root = (Parent) loader.load();

        ListaRecensioniController controller = (ListaRecensioniController) loader.getController();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        log.trace("Passo alla scena delle recensioni dell'utente");

        try {
            controller.init(listaRecensioni, utenteCorrente);
        } catch (ResourceNotFoundException e) {
            throw new IOException("Errore parametri passati non validi al controller ListaRecensioniController");
        }

        Stage current = (Stage) indietroButton.getScene().getWindow();
        current.close();

        stage.show();
    }

    /**
     * Quando clicco su Modifica utente mi abilita le textFields per le modifiche da apportare
     * @param actionEvent
     */
    public void onModifiedUser(ActionEvent actionEvent) {

        //Abilita le textFields per la modifica e i tasti Salva e Annulla
        nomeUtente.setEditable(true);
        cognomeUtente.setEditable(true);
        usernameUtente.setEditable(false);
        emailUtente.setEditable(true);

        salvaButton.setVisible(true);
        annullaButton.setVisible(true);

        //Disabilita gli altri tasti
        indietroButton.setDisable(true);
        modificaUtenteButton.setDisable(true);
        eliminaUtenteButton.setDisable(true);
        visualizzaRecensioniButton.setDisable(true);

    }

    /**
     * Cliccando sul tasto Salva, si prendono tutti i dati modificati e si manda richiesta a Cognito
     * @param actionEvent
     */
    public void onSaveAction(ActionEvent actionEvent) {

        //Se tutti i campi corretti e compilati
        if(checkFields()) {
            CognitoFactory cognito = new CognitoFactory();

            List<AttributeType> listaAttributi = cognito.getAttributeList();

            //Se lista presa allora avvia processo di update
            if (listaAttributi.size() > 0 || listaAttributi != null) {
                initUpdate(listaAttributi, cognito);
            }

            //update currentUser
            updateCurrentUser();

            //Show Information Dialog
            Dialogs.showInformationDialog("Modifica Avvenuta","Modifica avvenuta con successo.");

        }
        else {
            log.error("Campi non compilati correttamente");

            Dialogs.showErrorDialog("Errore Update","Campi non compilati correttamente");

            //RollBack
            rollbackParameters();
            }

            //Alla fine nascondi tasti
            nomeUtente.setEditable(false);
            cognomeUtente.setEditable(false);
            usernameUtente.setEditable(false);
            emailUtente.setEditable(false);

            //Nascondi i due buttons
            salvaButton.setVisible(false);
            annullaButton.setVisible(false);

            //Abilita gli altri tasti
            indietroButton.setDisable(false);
            modificaUtenteButton.setDisable(false);
            eliminaUtenteButton.setDisable(false);
            visualizzaRecensioniButton.setDisable(false);

    }

    /**
     * Metodo di supporto per l'update utente
     * @param listaAttributi
     * @param cognito
     */
    private void initUpdate(List<AttributeType> listaAttributi, CognitoFactory cognito) {

        //Modifico la nuova lista di attributi secondo i parametri passati nelle textFields

        try {
            listaAttributi.get(2).withName("name").setValue(nomeUtente.getText());
            listaAttributi.get(4).withName("family_name").setValue(cognomeUtente.getText());
            listaAttributi.get(3).withName("nickname").setValue(usernameUtente.getText());
            listaAttributi.get(5).withName("email").setValue(emailUtente.getText());

            //Remove attributes that can't update
            listaAttributi.remove(0); //Attributo sub non si può modificare
            listaAttributi.remove(0); //Attributo email_verified non conta

            cognito.updateUser(listaAttributi,utenteCorrente);
            log.info("Dati utente aggiornati");
        } catch (ConnectionActionsException e) {
            e.printStackTrace();
        }catch (ArrayIndexOutOfBoundsException e){
            log.error("Errore: parametri attributi null");
        }

    }

    /**
     * Annulla la modifica dell'utente
     * @param actionEvent
     */
    public void onAnnullaAction(ActionEvent actionEvent) {


        //Rollback
        rollbackParameters();

        //Disabilita la modifica dei parametri
        nomeUtente.setEditable(false);
        cognomeUtente.setEditable(false);
        usernameUtente.setEditable(false);
        emailUtente.setEditable(false);

        //Nascondi i due buttons
        salvaButton.setVisible(false);
        annullaButton.setVisible(false);

        //Abilita gli altri tasti
        indietroButton.setDisable(false);
        modificaUtenteButton.setDisable(false);
        eliminaUtenteButton.setDisable(false);
        visualizzaRecensioniButton.setDisable(false);

    }

    /**
     * Elimina l'utente quando clicchi sul pulsante
     * @param actionEvent
     */
    public void onDeleteButton(ActionEvent actionEvent) throws IOException {

        //Show confirmation Dialog
        Optional<ButtonType> result = Dialogs.getConfirmationDialog("Conferma Elimina Utente","Sei sicuro di voler eliminare l'utente selezionato ?");

        //Se viene cliccato il tasto OK...
        if (result.get() == ButtonType.OK){
            deleteUser();

            //Torna alla lista di utenti
            Parent root = (Parent) FXMLLoader.load(getClass().getResource("/Layout/RicercaUtenti.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(root));

            log.trace("Torno alla lista utenti");
            Stage current = (Stage) indietroButton.getScene().getWindow();
            current.close();
            stage.show();

        }
    }

    /**
     * Metodo di supporto del confirmation Dialog per l'eliminazione dell'utente
     */
    private void deleteUser() {

      CognitoFactory factory = new CognitoFactory();

        try {
            factory.deleteUser(utenteCorrente);
        } catch (ConnectionActionsException e) {
            log.error("Errore durante la chiamata per l'eliminazione dell'utente "+utenteCorrente.getUsername());
        }
    }

    /**
     * Quando si clicca sul tasto resetta password invia richiesta di reset password
     * @param actionEvent
     */
    public void onResetPassword(ActionEvent actionEvent) {

        //Mostra Messaggio di conferma
        Optional<ButtonType> result = Dialogs.getConfirmationDialog("Reset Password","Sei sicuro di voler resettare la password dell'utente ?");

        //Se viene cliccato il tasto OK...
        if (result.get() == ButtonType.OK){
            CognitoFactory factory = new CognitoFactory();

            try{
                log.trace("Avviata procedura di reset password");

                factory.resetPassword(utenteCorrente);

                log.info("Password resettata con successo");

            }catch (ConnectionActionsException e) {
                log.error("Errore: Reset Password non andato a buon fine");
            }
        }
    }

    /**
     * Verifica che tutte le textfields siano state riempite e rispettino la forma giusta
     * @return True se OK, False altrimenti
     */
    private boolean checkFields(){

        if(nomeUtente.getText().isEmpty()){ return false;}
        if (cognomeUtente.getText().isEmpty()) { return false;}
        if(usernameUtente.getText().isEmpty()){ return false;}
        if(emailUtente.getText().isEmpty()){ return false;}

        //Verifica se la stringa email è nel formato valido
        if(!isValidEmailAddress(emailUtente.getText())){
            Dialogs.showErrorDialog("Errore email","Errore: inserita una email non valida");
            return false;
        }

        return true;
    }

    /**
     * Verifica se l'email è strutturata in modo corretto
     * @param email email address passato per la modifica
     * @return True se l'email è passata nel formato corretto
     */
    private boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();

        } catch (AddressException ex) {
            log.error("Attenzione immessa email con formato non valido!");
            return false;
        }
        return result;
    }

    /**
     * Imposta i parametri precedenti prima della modifica
     */
    private void rollbackParameters(){
        //Rollback parametri iniziali
        nomeUtente.setText(utenteCorrente.getNome());
        cognomeUtente.setText(utenteCorrente.getCognome());
        usernameUtente.setText(utenteCorrente.getUsername());
        emailUtente.setText(utenteCorrente.getEmail());
    }

    /**
     * Aggiorna utenteCorrente
     */
    private void updateCurrentUser(){
        if(utenteCorrente != null){
            utenteCorrente.setNome(nomeUtente.getText());
            utenteCorrente.setCognome(cognomeUtente.getText());
            utenteCorrente.setUsername(usernameUtente.getText());
            utenteCorrente.setEmail(emailUtente.getText());
        }
    }


}
