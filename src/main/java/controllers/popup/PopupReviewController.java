package controllers.popup;

import amazoneservices.dynamodb.DynamoFactory;
import amazoneservices.dynamodb.exceptions.DynamoOperationException;
import controllers.ListaRecensioniController;
import exceptions.ResourceNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import model.Reviews;
import org.controlsfx.control.Rating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.components.Dialogs;

public class PopupReviewController {


    private Logger log = LoggerFactory.getLogger(PopupReviewController.class);
    private Reviews recensione;
    private DynamoFactory dynamo;

    @FXML private Button deleteReviewButton;
    @FXML private Button closePopupButton;
    @FXML private Label titoloLabel;
    @FXML private Label dataRecensioneLabel;
    @FXML private Label ultimaVisitaLabel;
    @FXML private TextArea commentoText;

    @FXML private Rating ratingReview;


    public void execute(Reviews reviews) throws ResourceNotFoundException {
        if(reviews == null){
            log.error("Errore: Recensione null passata al controller");
            throw new ResourceNotFoundException("Errore la recensione passata non può essere null");
        }

        recensione = reviews;

        dynamo = new DynamoFactory();

        titoloLabel.setText(recensione.getTitoloRecensione());
        dataRecensioneLabel.setText(recensione.getDataRecensione());
        ultimaVisitaLabel.setText(recensione.getDataUltimaVisita());
        commentoText.setText(recensione.getCommento());
        ratingReview.setRating(recensione.getValutazioneRecensione());
    }

    /**
     * Elimina la recensione presente mostrata nel popup
     * @param actionEvent
     */
    public void onClickDeleteReview(ActionEvent actionEvent) {

        try {
            log.trace("Chiamo la delete sulla recensione...");
            dynamo.deleteReview(recensione);

            log.info("Recensione eliminata con successo, chiudo il popup");

            Dialogs.showInformationDialog("Recensione eliminata","La recensione è stata eliminata con successo.");

            log.trace("Chiusura con eliminazione...");

            ListaRecensioniController recensioniController = ListaRecensioniController.getRecensioniController();

            //lancio il metodo refresh
            recensioniController.refreshTable(recensione);

            Stage current = (Stage) closePopupButton.getScene().getWindow();
            current.close();

        } catch (DynamoOperationException e) {
            log.error("Errore durante l'eliminazione della recensione da dynamoDB");
        }
    }

    /**
     * Chiudi il popup
     * @param actionEvent
     */
    public void closePopupScene(ActionEvent actionEvent) {

        log.trace("Chiudo il popup senza eliminare la recensione");

        Stage current = (Stage) closePopupButton.getScene().getWindow();
        current.close();

    }
}
