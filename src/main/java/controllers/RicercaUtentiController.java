package controllers;

import amazoneservices.cognito.CognitoFactory;
import amazoneservices.cognito.connection.CognitoAuthentication;
import amazoneservices.cognito.exceptions.ConnectionActionsException;
import exceptions.ViewNotFoundException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RicercaUtentiController implements Initializable {

    private static Logger log = LoggerFactory.getLogger(RicercaUtentiController.class);

    private ObservableList<Users> listaUtenti = FXCollections.observableArrayList();
    private FilteredList<Users> filteredList;
    private Users userSelected;

    @FXML private Button visualizzaButton;

    @FXML private TableView<Users> usersTableView;

    @FXML private TableColumn surnameColumn;
    @FXML private TableColumn nameColumn;
    @FXML private TableColumn usernameColumn;
    @FXML private TableColumn emailColumn;

    @FXML private TextField searchText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("cognome"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("nome"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));

        buildUsersTable();

    }

    /**
     * Popola la tabella degli utenti
     */
    private void buildUsersTable(){
        // Get lista utenti da Cognito
        try{
            CognitoFactory cognito = new CognitoFactory();

            listaUtenti.addAll(cognito.listAllUsers(60));

            filteredList = new FilteredList<Users>(listaUtenti,b -> true);

            if(listaUtenti == null){
                throw new ConnectionActionsException("Errore durante la chiamata a cognito");
            }

            if(listaUtenti.size() == 0){
                log.warn("Lista utenti vuota");
            }
            //Se lista non vuota, aggiungi alla tabella gli utenti
            else {
                for(Users utente : listaUtenti){
                    usersTableView.getItems().add(utente);
                }

                //Aggiunti Observable per la selezione degli utenti
                usersTableView.getSelectionModel().selectedItemProperty().addListener(((observable, oldSelection, newSelection) -> {
                    if(newSelection != null){
                        //Cattura l'utente selezionato
                        userSelected = newSelection;

                        //Abilita il pulsante visualizza
                        visualizzaButton.setDisable(false);

                    }
                }));

            }

        }catch (Exception e){
            log.error("Errore durante la chiamata a Cognito per la get della lista utenti");
        }
    }

    /**
     * Metodo per la ricerca dinamica dell'utente sulla barra di ricerca in alto
     * @param keyEvent
     */
    public void onKeyPressed(KeyEvent keyEvent) {

        // 2. Set the filter Predicate whenever the filter changes.
        searchText.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(employee -> {

                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (employee.getCognome().toLowerCase().indexOf(lowerCaseFilter) != -1 ) {
                    return true; // Filter matches first name.
                }
                else
                    return false; // Does not match.
            });
        });

        //  Wrap the FilteredList in a SortedList.
        SortedList<Users> sortedData = new SortedList<>(filteredList);

        //    Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(usersTableView.comparatorProperty());

        // Add sorted (and filtered) data to the table.
        usersTableView.setItems(sortedData);

    }


    /**
     * Passa al profilo dell'utente passandogli come parametro l'object Users
     * @param actionEvent
     */
    public void onClick(ActionEvent actionEvent) throws ViewNotFoundException {

        if(userSelected != null){
                try{

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Layout/ProfiloUtente.fxml"));
                    Parent root = (Parent) loader.load();

                    ProfiloUtenteController controller = (ProfiloUtenteController) loader.<ProfiloUtenteController>getController();

                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));

                    log.info("Entro nel profilo di: "+userSelected.getUsername());

                    controller.initialize(userSelected);

                    stage.show();

                    Stage current = (Stage) visualizzaButton.getScene().getWindow();
                    current.close();


                }catch (IOException e){
                    throw new ViewNotFoundException("Errore durante il caricamento della scene profilo utente");
                }
        }
        else {
            log.error("Nessun utente selezionato da poter passare alla scena successiva");
        }
    }
}
