package controllers;

import controllers.popup.PopupReviewController;
import exceptions.ResourceNotFoundException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Reviews;
import model.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class ListaRecensioniController {

    private Logger log = LoggerFactory.getLogger(ListaRecensioniController.class);

    private static ListaRecensioniController recensioniController=null;

    private List<Reviews> reviewsList;
    private Users currentUser;
    private Reviews selectedReview;

    @FXML private Button visualizzaButton;
    @FXML private Button indietroButton;

    @FXML private TableView<Reviews> reviewsTableView;
    @FXML private TableColumn strutturaColumn;
    @FXML private TableColumn titoloColumn;
    @FXML private TableColumn votoColumn;
    @FXML private TableColumn visitaColumn;

    @FXML private Label fullnameUserLabel;

    /**
     * Inizializzo il controller passando utente e recensioni dell'utente
     */
    public void init(List<Reviews> listaReview, Users utente) throws ResourceNotFoundException {
        if(listaReview.size() == 0){
            log.warn("Lista vuota per l'utente "+utente.getUsername());
        }
        if(utente == null){
            throw new ResourceNotFoundException("Errore utente non passato correttamente");
        }

        recensioniController = this;

        this.reviewsList = listaReview;
        currentUser = utente;

        //Setta nome e cognome in altro a dx
        fullnameUserLabel.setText(currentUser.getNome()+" "+currentUser.getCognome());

        //Setta i tipi di campi delle colonne
        strutturaColumn.setCellValueFactory(new PropertyValueFactory<>("nomeStruttura"));
        titoloColumn.setCellValueFactory(new PropertyValueFactory<>("titoloRecensione"));
        votoColumn.setCellValueFactory(new PropertyValueFactory<>("valutazioneRecensione"));
        visitaColumn.setCellValueFactory(new PropertyValueFactory<>("dataUltimaVisita"));

        //Disattiva il pulsante Visualizza fino alla selezione
        visualizzaButton.setDisable(true);


        //Passo al metodo che esegue i processi
        execute();
    }

    /**
     * Metodo per l'esecuzione dei processi dopo l'inizializzazione della classe controller
     */
    private void execute() {

        //Aggiungi alla tabella i dati recuperati dalla lista
        for(Reviews review : reviewsList){
            reviewsTableView.getItems().add(review);
        }

        //Aggiunti Observable per la selezione delle recensioni
        reviewsTableView.getSelectionModel().selectedItemProperty().addListener(((observable, oldSelection, newSelection) -> {
            if(newSelection != null){

                //Cattura la recensione selezionata
                selectedReview = newSelection;

                if(selectedReview != null){

                    visualizzaButton.setDisable(false);

                    log.info("Recensione selezionata: "+selectedReview.getTitoloRecensione());
                }

            }
        }));

    }

    /**
     * Mostra la recensione selezionata dalla lista
     * @param actionEvent
     * @throws IOException
     * @throws ResourceNotFoundException
     */
    public void onClickView(ActionEvent actionEvent) throws IOException, ResourceNotFoundException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Popup/popupReview.fxml"));
        Parent root = (Parent) loader.load();

        PopupReviewController controller = (PopupReviewController) loader.getController();

        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        log.info("Apro la recensione: "+selectedReview.getTitoloRecensione());

        controller.execute(selectedReview);

        stage.show();

    }

    /**
     * Torna al profilo dell'utente
     * @param actionEvent
     * @throws IOException
     */
    public void onBackReview(ActionEvent actionEvent) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Layout/ProfiloUtente.fxml"));

        Parent root = (Parent) loader.load();

        ProfiloUtenteController controller = (ProfiloUtenteController) loader.getController();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        log.trace("Torno alla scena precedente");
        controller.initialize(currentUser);

        Stage current = (Stage) indietroButton.getScene().getWindow();
        current.close();

        stage.show();
    }

    /**
     * Ricarica la tabella ogni volta che
     * @param reviewDeleted Recensione eliminata
     */
    public void refreshTable(Reviews reviewDeleted) {
        log.trace("Refreshing Table...");

        reviewsList.remove(reviewDeleted);

        //Clear table result
        reviewsTableView.getItems().clear();

        execute();
    }

    /**
     * Ottieni istanza del controller per lanciare metodi dall'esterno
     * @return Istanza del controller
     */
    public static ListaRecensioniController getRecensioniController(){
        if(recensioniController == null){
            recensioniController = new ListaRecensioniController();
        }
        return recensioniController;
    }
}
